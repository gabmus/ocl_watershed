#!/usr/bin/env python3

import json

F_PRE = './benchmark'
F_PST = '.json'

files = [
    '_gtx_1050ti_mobile',
    '_gtx_760',
    '_gtx_960',
    '_intel_630',
]

sizes = {
    'toronto': { 'width': 4201, 'height': 3001 },
    'grass': { 'width': 3456, 'height': 2160 }
}

def fix_obj(obj):
    if obj['implementation'] in ["global", "image"]:
        return obj
    else:
        n_wg = int((sizes[obj['image']]['width']*sizes[obj['image']]['height'])/(int(obj['size'])*int(obj['size'])))
        n_bytes = (((6*int(obj['size'])*int(obj['size'])) + (8*int(obj['size'])))*n_wg)*4
        thr = n_bytes/(float(obj['time'])*1000.0)
        obj['throughput'] = format(thr, '.3f')
        return obj

for f in files:
    print(f)
    out = ''
    with open('{0}{1}{2}'.format(F_PRE, f, F_PST), "r") as fd:
        b = json.loads(fd.read()[len('var bench{0}='.format(f)):])
        for index, obj in enumerate(b['benchmark']):
            b['benchmark'][index] = fix_obj(obj)
        out = 'var bench{0}={1}'.format(f, json.dumps(b))
        fd.close()
    with open('{0}{1}{2}'.format(F_PRE, f, F_PST), "w") as fd:
        fd.write(out)
        fd.close()
